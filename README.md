# Fly.FR [![Build Status](https://travis-ci.org/aminembarki/fly-fr.svg)](https://travis-ci.org/aminembarki/fly-fr)
DESCRIPTION.

[Download Fly.FR](https://bitbucket.org/flyfr/fly-front/downloads) / [View the demo](http://web.fly.fr/fly-fr/)



## Getting Started

Compiled and production-ready code can be found in the `dist` directory. The `src` directory contains development code. Unit tests are located in the `test` directory.

### 1. Include Fly.FR on your site.

```html
<link rel="stylesheet" href="dist/css/fly-fr.css">
<script src="dist/js/fly-fr.js"></script>
```

### 2. Add the markup to your HTML.

Details...

```html
Markup here...
```

### 3. Initialize Fly.FR.

In the footer of your page, after the content, initialize Fly.FR. And that's it, you're done. Nice work!

```html
<script>
	fly-fr.init();
</script>
```



## Installing with Package Managers

You can install NAMEPSACE-UP with your favorite package manager.

* **NPM:** `npm install `
* **Bower:** `bower install https://github.com/aminembarki/fly-fr.git`
* **Component:** `component install aminembarki/fly-fr`



## Working with the Source Files

If you would prefer, you can work with the development code in the `src` directory using the included [Gulp build system](http://gulpjs.com/). This compiles, lints, and minifies code, and runs unit tests.

### Dependencies
Make sure these are installed first.

* [Node.js](http://nodejs.org)
* [Gulp](http://gulpjs.com) `sudo npm install -g gulp`

### Quick Start

1. In bash/terminal/command line, `cd` into your project directory.
2. Run `npm install` to install required files.
3. When it's done installing, run one of the task runners to get going:
	* `gulp` manually compiles files.
	* `gulp watch` automatically compiles files and applies changes using [LiveReload](http://livereload.com/).
	* `gulp test` compiles files and runs unit tests.



## Options and Settings

Fly.FR includes smart defaults and works right out of the box. But if you want to customize things, it also has a robust API that provides multiple ways for you to adjust the default options and settings.

### Global Settings

You can pass options and callbacks into Fly.FR through the `init()` function:

```javascript
fly-fr.init({
	// Settings go here...
});
```

### Override settings with data attributes

Fly.FR also lets you override global settings on a link-by-link basis using the `[data-options]` data attribute:

```html
// Markup here...
```

### Use Fly.FR events in your own scripts

You can also call Fly.FR events in your own scripts.

#### functionName()
Description

```javascript
// Functions here...
```

**Example**

```javascript
// Example here...
```

#### destroy()
Destroy the current `fly-fr.init()`. This is called automatically during the `init` function to remove any existing initializations.

```javascript
fly-fr.destroy();
```



## Browser Compatibility

Fly.FR works in all modern browsers, and IE 9 and above.

Fly.FR is built with modern JavaScript APIs, and uses progressive enhancement. If the JavaScript file fails to load, or if your site is viewed on older and less capable browsers, all content will be displayed by default.



## How to Contribute

In lieu of a formal style guide, take care to maintain the existing coding style. Please apply fixes to both the development and production code. Don't forget to update the version number, and when applicable, the documentation.



## License

The code is available under the [MIT License](LICENSE.md).
(function ($, ui) {
    'use strict';

    $(function () {

        $('#getPositionButton').on ('click', function () {
            $.geolocation.get ({
                win: function (position) {
                    console.log ("Your position is " + position.coords.latitude + ", " + position.coords.longitude);
                }, fail: function (error) {
                    console.log ("No location info available. Error code: " + error.code);
                }
            });
        });


        /**  modal Map */
        var LocsStores = [
            {
                lat: 45.9,
                lon: 10.9,
                title: 'Store A',
                html: [
                    '&lt;h3&gt;Adresse C1&lt;/h3&gt;',
                    '&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
                ].join (''),
                icon: 'http://128.199.60.141/img/common/POINTER_MAP.png',
                animation: google.maps.Animation.DROP,
                show_infowindow: false
            },
            {
                lat: 44.8,
                lon: 1.7,
                title: 'Store B',
                html: [
                    '&lt;h3&gt;Adresse C1&lt;/h3&gt;',
                    '&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
                ].join (''),
                icon: 'http://128.199.60.141/img/common/POINTER_MAP.png',
                show_infowindow: false
            },
            {
                lat: 51.5,
                lon: -1.1,
                title: 'Store C',
                html: [
                    '&lt;h3&gt;Adresse C1&lt;/h3&gt;',
                    '&lt;p&gt;Lorem Ipsum..&lt;/p&gt;'
                ].join (''),
                show_infowindow: false,
                icon: 'http://128.199.60.141/img/common/POINTER_MAP.png',
            }
        ];


        var maplace = new Maplace({
            map_div: '#dashboard-map-stores',
            start: 1,
            locations: LocsStores,
            generate_controls: false,
            styles: {
                'Greyscale': [{
                    featureType: 'all',
                    stylers: [
                        {saturation: -100},
                        {gamma: 0.50}
                    ]
                }]
            },
            map_options: {
                zoom: 5
            }
        }).Load ();
    });

})(jQuery, ui);


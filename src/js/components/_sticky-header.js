$.fn.isVisible = function() {
  // is Main Image visible ? if yes = addclass to Sticky Header to show it

  var rect = this[0].getBoundingClientRect();
  return (
    (rect.height > 0 || rect.width > 0) &&
    rect.bottom >= 0 &&
    rect.right >= 0 &&
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

function doCheck() {
  var elementToDetect = $('#header');
  var stickyHeader = $('#sticky-header');

  if (elementToDetect.isVisible()) {
    stickyHeader.removeClass('visible');
  } else {
    stickyHeader.addClass('visible');
  }

}

$(document)
  .ready(function(e){
    doCheck();
  });

$(window)
  .scroll(function(e){
    doCheck();
  });

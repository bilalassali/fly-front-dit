

(function ( $ , ui ) {
	'use strict';
	
	$ ( function () {
		
		$ ( 'li.credit-visu-option a' ).on ( 'click' , function () {
			$('li.credit-visu-option a .css-radio').prop("checked", false);
			$(this).find('.css-radio').prop("checked", true);
		} );

		$ ( 'div.credit-visu-option a' ).on ( 'click' , function () {
			$('div.credit-visu-option a .css-radio').prop("checked", false);
			$(this).find('.css-radio').prop("checked", true);
		} );
	} );
	
}) ( jQuery , ui );


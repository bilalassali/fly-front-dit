/**
 * Created by amine on 10/28/2016.
 */



(function($,ui){
	'use strict';
	
	$ ( function () {
		
		
		
		
		// Reset filter button
		$ ( '#reset-filter' ).click ( function () {
			$ ( ".block-filter input[type=checkbox]" ).prop ( 'checked' , false );
		} );
		
		// display-product-option sidebar mobile
		$ ( ".display-product-option" ).clone ().insertAfter ( ".side-nav-categories" ).addClass ( 'rendered' );
		
		// sidebar mobile nav
		$ ( '.sidebar .side-nav-categories .block-title' ).click ( function () {
			$ ( '.side-nav-categories .category-menu' ).collapse ( 'toggle' );
		} );
		
		// sidebar filter toggle
		$ ( '.show-filter-mobile' ).click ( function () {
			var block_filter = $ ( '.block-filter' );
			var icon_plus = $(this).find('.icon-plus');
			block_filter.collapse ( 'toggle' );
			block_filter.on('shown.bs.collapse', function(){
				icon_plus.html('-');
			});
			block_filter.on('hide.bs.collapse', function(){
				icon_plus.html('+');
			});
		} );
		
		// Render Product ON hover
		ui.renderProductsListe();


	} );
})(jQuery,ui);